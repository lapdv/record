//
//  RmAds.m
//  GooRec
//
//  Copyright © 2021 Progg. All rights reserved.
//

#import "RmAds.h"

@interface RmAds () <UITableViewDelegate, UITableViewDataSource>
@property(weak, nonatomic) IBOutlet UITableView *tbView;
@property(strong, nonatomic) NSArray *content;
@end

NSString *inAppProductID = @"jp.co.goorecads";
NSString *inAppProductID3 = @"jp.co.goorecads3";
NSString *inAppProductID5 = @"jp.co.goorecads5";
NSString *inAppProductID6 = @"jp.co.goorecads6";
NSString *inAppProductID7 = @"jp.co.goorecads7";
NSString *inAppProductID8 = @"jp.co.goorecads8";
NSString *inAppProductID9 = @"jp.co.goorecads9";
NSString *inAppProductID10 = @"jp.co.goorecads10";
NSArray *idArray;

@implementation RmAds

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Plan Manager";
    self.tbView.delegate = self;
    self.tbView.dataSource = self;
//    self.content = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];

    // Adding activity indicator
    activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
    activityIndicatorView.center = self.view.center;
    [activityIndicatorView hidesWhenStopped];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];

    //Hide purchase button initially
//    purchaseButton.hidden = YES;
    [self fetchAvailableProducts];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _content.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [self.tbView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text = [_content objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath; {
    NSLog(@"title of cell %@", [_content objectAtIndex:indexPath.row]);
    int rowID = (int) indexPath.row;
    [activityIndicatorView startAnimating];
    [self purchaseMyProduct:[validProducts objectAtIndex:rowID]];
    [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(henGio:) userInfo:nil repeats:NO];
}

//-------IAP
- (void)fetchAvailableProducts {
    idArray = @[inAppProductID, inAppProductID3, inAppProductID5, inAppProductID6, inAppProductID7, inAppProductID8, inAppProductID9, inAppProductID10];
    NSSet *productIDs = [NSSet setWithArray:idArray];//[NSSet setWithObjects:inAppProductID, nil];
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIDs];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct *)product {
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Purchases are disabled in your device" message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

/**
 * Sau khi chon 1 trong cac IAP
 * @param queue
 * @param transactions
 */
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                for (NSString *identifier in idArray) {
                    if ([transaction.payment.productIdentifier isEqualToString:identifier]) {
                        NSLog(@"Purchased ");
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Very good" message:@"Purchase is completed successfully" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
                        [alert addAction:defaultAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

/**
 * Callback when open app -> query
 */
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    SKProduct *validProduct = nil;
    if ([response.products count] > 0) {
        validProducts = response.products;
        NSMutableArray *danhSachIap = [[NSMutableArray alloc] init];
        for (validProduct in validProducts) {
            for (NSString *identifier in idArray) {
                if ([validProduct.productIdentifier isEqualToString:identifier]) {
                    [danhSachIap addObject:validProduct.localizedTitle];
                    NSLog(@"hihi Product Title: %@", validProduct.localizedTitle);
                    NSLog(@"Product productIdentifier: %@", validProduct.productIdentifier);
                    NSLog(@"Product Desc: %@", validProduct.localizedDescription);
                    NSLog(@"Product price: %@", validProduct.price);
                    NSLog(@"Product priceLocale: %@", validProduct.priceLocale);
                    NSLog(@"Product subscriptionPeriod: %@", validProduct.subscriptionPeriod);
                    NSLog(@"---------------------------");
                }
            }
        }
        self.content = [danhSachIap copy];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tbView reloadData];
        });
    } else {
        UIAlertView *tmp = [[UIAlertView alloc] initWithTitle:@"Not Available" message:@"No products to purchase" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [tmp show];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [activityIndicatorView stopAnimating];
    });
//    purchaseButton.hidden = NO;
}

-(void)henGio:(NSTimer *)timer {
    [activityIndicatorView stopAnimating];
}
//-------End IAP
@end
