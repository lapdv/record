//
//  BeginRecVC.m
//  Recorder
//
//  Created by Prog on 3/24/13.
//  Copyright (c) Prog on 2021. All rights reserved.
//

#import "utilities.h"
#import "CCRecording.h"
#import "BeginRecVC.h"

@interface BeginRecVC () {
    BOOL isRecoding;
    CCRecording *currentRecording;
}
@property (weak, nonatomic) IBOutlet UIButton *rmAds;
@property (weak, nonatomic) IBOutlet UILabel *statLbl;
- (IBAction)toggleRecording:(UIButton *)sender;

- (IBAction)playRecording:(UIBarButtonItem *)sender;

- (IBAction)pauseRecording:(UIBarButtonItem *)sender;

- (IBAction)deleteRecording:(UIBarButtonItem *)sender;
- (IBAction)rmAdBtn:(id)sender;
@end

@implementation BeginRecVC

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isRecoding = NO;
    }
    return self;
}

- (void)dealloc {
    currentRecording = nil;
}

#pragma mark -
#pragma mark Button Actions

- (void)toggleRecording:(UIButton *)sender {
    isRecoding = !isRecoding;
    UIImage *btnImage;
    if (isRecoding) {
        [_statLbl setText:@"Recording..."];
        btnImage = [UIImage imageNamed:@"rec_on.png"];
    } else {
        btnImage = [UIImage imageNamed:@"rec_off.png"];
        [_statLbl setText:@"Start"];
    }
    [sender setImage:btnImage forState:UIControlStateNormal];
    if (isRecoding) {
        NSDate *now = [NSDate date];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"de-DE"];
        format.locale = locale;
        format.dateStyle = NSDateFormatterMediumStyle;
        format.timeStyle = NSDateFormatterMediumStyle;
        NSString *filename = [format stringFromDate:now];
        currentRecording = [[CCRecording alloc] initWithName:filename andAudioData:[NSData data]];
        [currentRecording startRecording];
    } else {
        [currentRecording stopRecording];
        UIAlertView *filenameDialog = [[UIAlertView alloc] initWithTitle:@"Enter a filename :" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Save", nil];
        filenameDialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [filenameDialog textFieldAtIndex:0].text = currentRecording.name;
        [filenameDialog show];
    }
    NSLog(@"Recoding was %@ from %@", isRecoding ? @"started" : @"stopped", sender);
}

- (IBAction)playRecording:(UIBarButtonItem *)sender {
    if (currentRecording) {
        [currentRecording startPlayback];
    }
}

- (IBAction)pauseRecording:(UIBarButtonItem *)sender {
    if (currentRecording) {
        [currentRecording pausePlayback];
    }
}

- (IBAction)rmAdBtn:(id)sender {
    NSLog(@"rm thiss");
}

- (IBAction)deleteRecording:(UIBarButtonItem *)sender {
    if (currentRecording) {
        [currentRecording deleteFile];
    }
}

#pragma mark -
#pragma mark UIAlertViewDelegate protocol implemetation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *filename = [alertView textFieldAtIndex:0].text;
    currentRecording.name = filename;
    [currentRecording saveFile];
}

@end
