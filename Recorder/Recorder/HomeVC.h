//
//  CCViewController.h
//  Recorder
//
//  Created by Prog on 16/12/2021.
//  Copyright (c) Prog on 2021. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCRecording.h"

@interface HomeVC : UITableViewController <UITableViewDataSource, UIAlertViewDelegate, CCRecordingDelegate> {

}

@property(strong, nonatomic) IBOutlet UITableView *tableView;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *editModeToggle;

- (IBAction)previousEntryWithButton:(UIBarButtonItem *)sender;

- (IBAction)nextEntryWithButton:(UIBarButtonItem *)sender;

- (IBAction)playCurrentItemWithButton:(UIBarButtonItem *)sender;

- (IBAction)pausePlayingItemWithButton:(UIBarButtonItem *)sender;

@end
