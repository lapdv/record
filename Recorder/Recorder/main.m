//
//  main.m
//  Recorder
//
//  Created by Prog on 16/12/2021.
//  Copyright (c) Prog on 2021. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
