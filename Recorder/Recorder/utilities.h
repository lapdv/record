//
//  utilities.h
//  Recorder
//
//  Created by Prog on 2021.
//  Copyright (c) Prog on 2021. All rights reserved.
//

#ifndef Recorder_utilities_h
#define Recorder_utilities_h

#define AppDelegate ((CCAppDelegate *)[UIApplication sharedApplication].delegate)

#ifdef DEBUG
#define DebugLog(formatString, args...) NSLog(formatString, args);
#else
#define DebugLog(formatString, args...)
#endif

#endif
