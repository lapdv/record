//
//  CCAppDelegate.h
//  Recorder
//
//  Created by Prog on 16/12/2021.
//  Copyright (c) Prog on 2021. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@end
