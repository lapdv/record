// Created by Prog on 12/21/21, 7:18 PM
// Copyright (c) 2021. GooRec Inc. All rights reserved.
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol IAPHelperDelegate <NSObject>
@required
//Required Delegate Method

@optional
//Optional Delegate Method
- (void)purchaseCompleteWithStatus:(NSInteger)status;

- (void)availableProducts;
@end

@interface IAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProductsRequest *productsRequest;
    NSSet *productIdentifiers;
    NSMutableDictionary *productsPrice;
    NSString *currentProductID;
    NSMutableArray *productsPurchased;
}

@property(nonatomic, weak) id <IAPHelperDelegate> delegate;
@property(nonatomic, strong) NSArray *validProducts;


+ (IAPHelper *)sharedIAP;

- (BOOL)isInternetAvailable;

- (void)initProductIdentifiers:(NSArray *)idArray;

- (void)fetchAvailableProducts;

- (BOOL)canMakePurchases;

- (void)purchaseMyProduct:(SKProduct *)product;

- (IBAction)purchase:(id)sender;

typedef enum {
    PURCHASE_STATUS_COMPLETED = 1,
    PURCHASE_STATUS_RESTORED,
    PURCHASE_STATUS_FAILED,
    PURCHASE_STATUS_CANCELLED

} PURCHASE_STATUS;

@end
