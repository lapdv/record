//
//  RmAds.h
//  GooRec
//
//  Copyright © 2021 Progg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface RmAds : UIViewController <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
}

- (void)fetchAvailableProducts;

- (BOOL)canMakePurchases;

- (void)purchaseMyProduct:(SKProduct *)product;
@end
